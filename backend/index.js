module.exports = (app, callback) => {
  const router = app.get('router')()
  const uuidv4 = require('uuid/v4')
  var db = require('diskdb')
  const stripe = require('stripe')

  db = db.connect(`${__dirname}`, ['payPages'])

  router.get('/configuration', (req, res) => {
    console.log('record found', req.query.uuid, db.payPages.findOne({ uuid: req.query.uuid }))
    res.json(db.payPages.findOne({ uuid: req.query.uuid }))
  })

  router.post('/paypage', (req, res) => {
    console.log('debug request', req.body, req)
    let uuid = uuidv4()
    db.payPages.save(Object.assign({}, req.body, { uuid: uuid }))
    res.json({ uuid: uuid })
  })

  router.post('/charge', (req, res) => {
    if (!req.body.token) return res.status(422).send('No token supplied to charge')
    const { id } = req.body.token
    const uuid = req.body.uuid
    if (!id) return res.status(422).send('No ID supplied to charge')
    const payPage = db.payPages.findOne({ uuid: uuid })

    return stripe(payPage.stripeSecretKey).charges.create(
      {
        amount: payPage.paymentAmount * 100,
        currency: 'usd',
        source: id,
        description: payPage.paymentDescription
      },
      error => {
        if (error) return res.status(error.status || 500).send({ error })

        return res.status(200).send({
          status: 200,
          message: 'Successfully charged.'
        })
      }
    )
  })

  return callback(null, router)
}
