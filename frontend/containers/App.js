import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import { connect } from 'react-redux'

import StripeCheckout from 'react-stripe-checkout'

import { fetchPaymentRequest, chargePaymentRequest } from '../actions/payment'

class App extends Component {
  constructor(props) {
    super(props)
    this.getQueryVariable = this.getQueryVariable.bind(this)
    this.props.fetchPaymentRequest(this.getQueryVariable('uuid'))
  }

  onToken = token => {
    this.props.chargePaymentRequest({ token, uuid: this.getQueryVariable('uuid') })
  }

  getQueryVariable = variable => {
    let query = window.location.search.substring(1)
    let vars = query.split('&')
    for (let i = 0; i < vars.length; i++) {
      let pair = vars[i].split('=')
      if (decodeURIComponent(pair[0]) == variable) {
        return decodeURIComponent(pair[1])
      }
    }
    console.log('Query variable %s not found', variable)
  }

  render() {
    const { payment: { payment, loaded, loading } } = this.props
    console.log('rendering...', payment)
    const styles = {
      container: {
        padding: 100,
        color: '#111',
        maxWidth: 640
      },
      containerText: {
        fontSize: 40,
        lineHeight: 1.4
      },
      payNow: {
        backgroundColor: '#aaa',
        color: '#fff',
        padding: '16px 24px',
        fontSize: 20,
        border: 0,
        borderRadius: 4,
        fontWeight: 700,
        cursor: 'pointer'
      },
      paymentPerson: {
        color: '#FF4136'
      },
      paymentAmount: {
        color: '#2ECC40'
      },
      paymentDescription: {
        color: '#0074D9'
      }
    }

    return (
      <div style={styles.container}>
        {loading && <p>The payment is loading.</p>}
        {loaded && !payment && <p>There was an error loading.</p>}
        {loaded && payment.status === 200 && <span>Payment sent! 🎉</span>}
        {payment.message && <p style={styles.containerText}>{payment.paymentDescription}</p>}
        {payment &&
          payment.stripePublicKey && (
            <div>
              <p style={styles.containerText}>
                Hi, <span style={styles.paymentPerson}>{payment.paymentPerson}</span>.
                <br />
                <br />
                You’re about to pay{' '}
                <span style={styles.paymentAmount}>${payment.paymentAmount}</span> for{' '}
                <span style={styles.paymentDescription}>{payment.paymentDescription}</span>.
                <br />
                <br />
                <StripeCheckout
                  token={this.onToken}
                  stripeKey={payment.stripePublicKey}
                  amount={payment.paymentAmount * 100}
                  name={`Paying for ${payment.paymentDescription}`}
                >
                  <button style={styles.payNow}>{payment.buttonText}</button>
                </StripeCheckout>
              </p>
            </div>
          )}
      </div>
    )
  }
}

App.propTypes = {
  payment: PropTypes.shape({
    payment: PropTypes.object,
    loaded: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired,
  fetchPaymentRequest: PropTypes.func.isRequired,
  chargePaymentRequest: PropTypes.func.isRequired
}

export default connect(({ payment }) => ({ payment }), {
  fetchPaymentRequest,
  chargePaymentRequest
})(App)
