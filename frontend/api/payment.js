function createApiRequest(url, method, data) {
  return fetch(url, {
    method,
    body: data ? JSON.stringify(data) : null,
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .catch(error => {
      throw error
    })
}

export function fetchPaymentApi(uuid) {
  return createApiRequest(`/api/configuration?uuid=${uuid}`, 'GET', null)
}

export function chargePaymentApi(token, uuid) {
  return createApiRequest('/api/charge', 'POST', { token, uuid })
}
