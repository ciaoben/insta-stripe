import { GET_PAYMENT_REQUEST, CHARGE_PAYMENT_REQUEST } from '../constants/payment'

export function fetchPaymentRequest(uuid) {
  return {
    type: GET_PAYMENT_REQUEST,
    uuid
  }
}

export function chargePaymentRequest({ token, uuid }) {
  return {
    type: CHARGE_PAYMENT_REQUEST,
    token,
    uuid
  }
}
